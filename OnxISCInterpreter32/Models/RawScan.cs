﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnxISCInterpreter32 {
  public class RawScan {
    public Oscandata oScanData { get; set; }
    public Oscanrefdata oScanRefData { get; set; }
  }

  public class Oscandata {
    public byte[] oScanIndex { get; set; }
    public string sScanName { get; set; }
    public string sScanType { get; set; }
    public string sScanDate { get; set; }
    public string sScanPacketFormatVer { get; set; }
    public int iScanDataSize { get; set; }
    public byte[] oScanData { get; set; }
  }

  public class Oscanrefdata {
    public byte[] oRefCalibrationCoefficients { get; set; }
    public int iRefCalibrationCoefficientSize { get; set; }
    public byte[] oRefCalibrationMatrix { get; set; }
    public int iRefCalibrationMatrixSize { get; set; }

  }
}
