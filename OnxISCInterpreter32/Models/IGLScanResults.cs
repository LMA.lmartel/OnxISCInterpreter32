﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace OnxISCInterpreter32.Models {
  public class IGLScanResults {

    public struct ScanResults: ICloneable {
      public uint header_version;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
      public string scan_name;
      public byte year;
      public byte month;
      public byte day;
      public byte day_of_week;
      public byte hour;
      public byte minute;
      public byte second;
      public short system_temp_hundredths;
      public short detector_temp_hundredths;
      public ushort humidity_hundredths;
      public ushort lamp_pd;
      public uint scanDataIndex;
      public CalibCoeffs calibration_coeffs;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
      public string serial_number;
      public ushort adc_data_length;
      public byte black_pattern_first;
      public byte black_pattern_period;
      public byte pga;
      public SlewScanConfig cfg;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 864)]
      public double[] wavelength;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 864)]
      public double[] intensity;
      public int length;

      public object Clone() {
        return this.MemberwiseClone();
      }
    }


    public struct CalibCoeffs {
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
      public double[] ShiftVectorCoeffs;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
      public double[] PixelToWavelengthCoeffs;
    }

    public struct SlewScanConfig {
      public SlewScanConfigHead head;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
      public SlewScanSection[] section;
    }

    public struct SlewScanSection {
      public byte section_scan_type;
      public byte width_px;
      public ushort wavelength_start_nm;
      public ushort wavelength_end_nm;
      public ushort num_patterns;
      public ushort exposure_time;
    }

    public struct SlewScanConfigHead {
      public byte scan_type;
      public ushort scanConfigIndex;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
      public string ScanConfig_serial_number;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
      public string config_name;
      public ushort num_repeats;
      public byte num_sections;
    }





  }
}