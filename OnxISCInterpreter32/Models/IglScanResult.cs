﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnxISCInterpreter32.Models {
  public class IglScanResult {
    public List<double> wavelength { get; set; }
    public List<double> reflectance { get; set; }
    public List<double> absorbance { get; set; }
    public List<double> scanDataIntensity { get; set; }
    public List<double> refDataIntensity { get; set; }

    //public int returnStatus { get; set; }

    public IglScanResult(List<double> wavelength, List<double> reflectance, List<double> absorbance, List<double> scanDataIntensity, List<double> refDataIntensity) {
      this.wavelength = wavelength ?? throw new ArgumentNullException(nameof(wavelength));
      this.reflectance = reflectance ?? throw new ArgumentNullException(nameof(reflectance));
      this.absorbance = absorbance ?? throw new ArgumentNullException(nameof(absorbance));
      this.scanDataIntensity = scanDataIntensity ?? throw new ArgumentNullException(nameof(scanDataIntensity));
      this.refDataIntensity = refDataIntensity ?? throw new ArgumentNullException(nameof(refDataIntensity));
      //this.returnStatus = returnStatus;
    }
  }
}