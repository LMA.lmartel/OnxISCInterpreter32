﻿using Newtonsoft.Json;
using OnxISCInterpreter32.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using static OnxISCInterpreter32.Models.IGLScanResults;

namespace OnxISCInterpreter32 {
  class Program : ICloneable {
    const string DLL_FILE_NAME = @"lib\libdlpspec_x32.dll";
    const string OUTPUT_FILE = @"assets\out\interpretedData.json";

    [DllImport(DLL_FILE_NAME, EntryPoint = "dlpspec_scan_interpret", CallingConvention = CallingConvention.Cdecl)]
    private static extern int IGLAPI_dlpspec_scan_interpret(byte[] oScanData, int iScanSize, out ScanResults oScanResults);

    //[DllImport(DLL_FILE_NAME, EntryPoint = "dlpspec_deserialize", CallingConvention = CallingConvention.Cdecl)]
    //private static extern int IGLAPI_dlpspec_deserialize(byte[] oScanData, int iScanSize, int iScanType);

    [DllImport(DLL_FILE_NAME, EntryPoint = "dlpspec_scan_read_data", CallingConvention = CallingConvention.Cdecl)]
    private static extern int IGLAPI_dlpspec_scan_read_data(byte[] pBuf, int bufSize);


    [DllImport(DLL_FILE_NAME, EntryPoint = "dlpspec_scan_interpReference", CallingConvention = CallingConvention.Cdecl)]
    private static extern int IGLAPI_dlpspec_scan_interpReference(
      byte[] oRefCalibrationData, int iRefCalibrationDataSize,
      byte[] oRefCalibrationMatrix, int iRefCalibrationMatrix,
      ScanResults oInterpretedScanResults, object oReferenceScanResults);

    static void Main(string[] args) {
      string sJsonFilePath = "", sJsonFile = "";
      RawScan oRawScan = null;
      if (args.Length < 1) {
        Console.WriteLine("[Erreur Syntaxe] > Le programme prends un argument le path d'un fichier JSON contenant les données de brutes du scan ");
      }

      sJsonFilePath = args[0];
      sJsonFile = File.ReadAllText(sJsonFilePath);
      oRawScan = JsonConvert.DeserializeObject<RawScan>(sJsonFile);
      int interpreted = interpretScanDataOld(oRawScan);

      Console.WriteLine("Interpreted = " + interpreted);

      Console.ReadLine();
    }

    [HandleProcessCorruptedStateExceptions, SecurityCritical]
    public static int interpretScanDataOld(RawScan oRawScan) {
      List<double> oWavelength = new List<double>();
      List<double> oReflectance = new List<double>();
      List<double> oAbsorbance = new List<double>();
      List<double> oScanDataIntensity = new List<double>();
      List<double> oRefDataIntensity = new List<double>();

      byte[] pScanDataBlob = oRawScan.oScanData.oScanData;
      byte[] pCopyBuff = null;
      byte[] pRefDataBlob = oRawScan.oScanRefData.oRefCalibrationCoefficients;
      byte[] pRefMatrixBlob = oRawScan.oScanRefData.oRefCalibrationMatrix;
      try {
        ScanResults oScanResults = new ScanResults();
        ScanResults oReferenceScanResults = new ScanResults();


        if (pScanDataBlob != null) {
          if (IGLAPI_dlpspec_scan_interpret(pScanDataBlob, pScanDataBlob.Length, out oScanResults) != 0) {
            return -1;
          }
        }
        //ScanResults oTmpScanResults = new ScanResults();
        //oTmpScanResults = (ScanResults)oScanResults.Clone();

        if (pRefDataBlob != null) {
          pCopyBuff = new byte[pScanDataBlob.Length];
          if (pCopyBuff == null) {
            return -2;
          }

          pCopyBuff = (byte[])pRefDataBlob.Clone();

          int retval = IGLAPI_dlpspec_scan_read_data(pCopyBuff, pScanDataBlob.Length);
          if (retval < 0) {
            return retval;
          }

          int tmp = IGLAPI_dlpspec_scan_interpReference(pRefDataBlob, pRefDataBlob.Length,
            pRefMatrixBlob, pRefMatrixBlob.Length, 
            oScanResults, null);

          //if (IGLAPI_dlpspec_scan_interpReference(pRefDataBlob, pScanDataBlob.Length,
          //  oRawScan.oScanRefData.oRefCalibrationMatrix, oRawScan.oScanRefData.oRefCalibrationMatrix.Length, oScanResults, oReferenceScanResults) != 0) {
          //  return -1;
          //}
          for (int i = 0; i < oScanResults.cfg.section[0].num_patterns; i++) {
            double iScanDataIntensity = oScanResults.intensity[i];
            double iRefDataIntensity = oReferenceScanResults.intensity[i];
            double iWavelength = oScanResults.wavelength[i];


            double iReflectance = (double)(iScanDataIntensity / iRefDataIntensity);
            double iAbsorbance = (double)((-1) * (Math.Log10(iReflectance)));


            Console.WriteLine(String.Format("Wavelength = {2} || Intensities : Sample = {0} || Reference {1} || Reflectance = {3} || Absorbance =  {4}",
              iScanDataIntensity,
              iRefDataIntensity,
              iWavelength,
              iReflectance,
              iAbsorbance
              ));

            //Debug.WriteLine(String.Format("Wavelength = {0} | Scan data intensity = {1} | Reference data intensity = {2} || reflectance = {3} | Absorbance = {4}",
            //  oScanResults.wavelength[i], iScanDataIntensity, iRefDataIntensity, iReflectance, iAbsorbance));
            oScanDataIntensity.Add(iScanDataIntensity);
            oRefDataIntensity.Add(iRefDataIntensity);
            oWavelength.Add(iWavelength);
            oReflectance.Add(iReflectance);
            oAbsorbance.Add(iAbsorbance);
          }

          IglScanResult oIglScanResult = new IglScanResult(oWavelength, oReflectance, oAbsorbance, oScanDataIntensity, oRefDataIntensity);

          File.WriteAllText(OUTPUT_FILE, JsonConvert.SerializeObject(oIglScanResult));
          if (!File.Exists(OUTPUT_FILE)) {
            return -5;
          }
        }
        return 0; // new IglScanResult(oWavelength, oReflectance, oAbsorbance, oScanDataIntensity, oRefDataIntensity);

      } catch (Exception e) {
        Console.WriteLine(e.Message);
        return -10;
      }
    }

    public object Clone() {
      return MemberwiseClone();
    }
  }


}
